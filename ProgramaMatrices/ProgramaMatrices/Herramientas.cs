﻿using System;

namespace ProgramaMatrices
{
    class Herramientas
    {
        /// <summary>
        /// Solicita un valor entero al usuario.
        /// </summary>
        /// <param name="mensaje">Texto que se mostrará antes del campo de entrada.</param>
        /// <returns>El valor ingresado; 0 en caso de no introdicir nada.</returns>
        public static int IngresarEntero(string mensaje)
        {
            Console.Write(mensaje);
            string valorIngresado = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(valorIngresado))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(valorIngresado);
            }
        }

        /// <summary>
        /// Solicita una cade al usuario.
        /// </summary>
        /// <param name="mensaje">Texto que se mostrará antes del campo de entrada.</param>
        /// <returns>Texto ingresado.</returns>
        public static string IngresarCadena(string mensaje)
        {
            Console.Write(mensaje);
            return Console.ReadLine();
        }

        /// <summary>
        /// Espera que se pulse una tecla para continuar con la ejecución del programa.
        /// </summary>
        public static void EsperarPulsacion()
        {
            Console.Write("\nPresione una tecla para continuar...");
            Console.ReadKey(true);
        }

        /// <summary>
        /// Solicita al usuario un valor entero mayor a cero.
        /// <paramref name="mensaje"/>Mensaje que se mostrará antes del campo de entrada.</param>
        /// </summary>
        /// <returns>Valor entero.</returns>
        public static int IngresarEnteroMayorA_0(string mensaje)
        {
            int numero;
            do
            {
                numero = IngresarEntero(mensaje);
            } while (numero <= 0);

            return numero;
        }

        public static void MostrarArrayEnteros(int[] array, string mensaje, string mensajePorLinea)
        {
            Console.WriteLine("\n" + mensaje);
            for (int l = 0; l < array.Length; l++)
            {
                Console.WriteLine(mensajePorLinea, l + 1, array[l]);
            }
        }

    }
}
