﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramaMatrices
{
    class Programa
    {
        static void Main(string[] args)
        {
            int opcion;

            do
            {
                Console.Clear();
                Console.WriteLine(
                    "Matrices C#\n" +
                    "\n" +
                    "1. Calcular sueldos de empleados\n" +
                    "2. Letra R con matrices NxN\n" +
                    "3. Número mayor y menor en una matriz NxN\n" +
                    "4 Ordenamiento de una matriz de NxN\n" +
                    "5. Valor mínimo de cada columna de una matriz NxN en un vector\n" +
                    "6. Valor máximo de cada fila de una matriz NxN en un vector\n" +
                    "7. Salir\n"
                    );

                opcion = Herramientas.IngresarEntero("Seleccione una opción: ");
                Console.WriteLine();

                switch (opcion)
                {
                    case 1:
                        Empleados.ProcesarEmpleados(4, 3);
                        break;
                    case 2:
                        Matrices.LetraR();
                        break;
                    case 3:
                        Matrices.ValorMayorMenorMatriz();
                        break;
                    case 4:
                        Matrices.OrdenarValores();
                        break;
                    case 5:
                        Matrices.ValoresMenoresColumnaMatriz();
                        break;
                    case 6:
                        Matrices.ValoresMayoresFilaMatriz();
                        break;
                    case 7:
                        Console.WriteLine("Sayonara Oni-Chan!.");
                        break;
                    default:
                        Console.WriteLine("Opción invalida!");
                        break;
                }

                Herramientas.EsperarPulsacion();

            } while (opcion != 7);

            
        }
    }
}
