﻿using System;

namespace ProgramaMatrices
{
    class Matrices
    {
        /// <summary>
        /// Muestra la letra R almacenada en una matriz;
        /// </summary>
        public static void LetraR()
        {
            const int columnas = 8;
            const int filas = 5;

            char[,] letra = new char[columnas, filas] {
                { 'R', 'R', 'R','R', ' ' },
                { 'R', ' ', ' ',' ', 'R' },
                { 'R', ' ', ' ',' ', 'R' },
                { 'R', ' ', ' ',' ', 'R' },
                { 'R', 'R', 'R','R', ' ' },
                { 'R', ' ', 'R',' ', ' ' },
                { 'R', ' ', ' ','R', ' ' },
                { 'R',' ', ' ', ' ', 'R' }

            };

            for (int c = 0; c < columnas; c++)
            {
                for (int f = 0; f < filas; f++)
                {
                    Console.Write(letra[c, f]);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Calcula el valor máximo y el mínimo de una matriz.
        /// </summary>
        public static void ValorMayorMenorMatriz()
        {
            // Declaración de variables y matriz;
            int columnas, filas, valorMayor, valorMenor, valor;
            int[,] valoresIngresados;

            // Asignación de datos;
            columnas = Herramientas.IngresarEnteroMayorA_0("Número de columnas: ");
            filas = Herramientas.IngresarEnteroMayorA_0("Filas: ");
            valorMayor = 0;
            valorMenor = 0;

            // Establecimiento del tamaño de la matriz de valores;
            valoresIngresados = new int[columnas, filas];

            for (int c = 0; c < columnas; c++)
            {
                Console.WriteLine($"\nColumna {c + 1}");

                for (int f = 0; f < filas; f++)
                {
                    valor = Herramientas.IngresarEntero(string.Format($"Valor {f + 1}: "));
                    valoresIngresados[c, f] = valor;

                    if (c == 0 && f == 0)
                    {
                        valorMayor = valor;
                        valorMenor = valor;
                    }
                    else if (valor > valorMayor)
                    {
                        valorMayor = valor;
                    }
                    else if (valor < valorMenor)
                    {
                        valorMenor = valor;
                    }
                }
            }

            Console.WriteLine(
                "\n" +
                $"El número mayor dentro de la matriz es {valorMayor}, mientras que el menor es {valorMenor}."
                );
        }

        /// <summary>
        /// Permite calcular el valor más bajo en una columna de una matriz.
        /// </summary>
        public static void ValoresMenoresColumnaMatriz()
        {
            Console.WriteLine("Calcula el valor mínimo de cada columna perteneciente a una matriz.\n");

            // Declaración de variables;
            int[,] valoresIngresados;
            int[] valoresMenores;
            int columnas, filas, valor, valorMenor = 0;

            // Obtiene el número de filas y columnas;
            columnas = Herramientas.IngresarEnteroMayorA_0("Número de columnas: ");
            filas = Herramientas.IngresarEnteroMayorA_0("Filas: ");

            // Establecimiento del tamaño de la matriz de valores y del array de los valores maximos;
            valoresIngresados = new int[columnas, filas];
            valoresMenores = new int[columnas];

            // Proceso columna por columna;
            for (int c = 0; c < columnas; c++)
            {
                Console.WriteLine($"\nColumna {c + 1}");

                // Obtiene los datos fila por fila;
                for (int f = 0; f < filas; f++)
                {
                    valor = Herramientas.IngresarEntero($"Valor {f + 1}: ");
                    valoresIngresados[c, f] = valor;

                    if (f == 0)
                    {
                        valorMenor = valor;
                    }
                    else if (valor < valorMenor)
                    {
                        valorMenor = valor;
                    }
                }

                valoresMenores[c] = valorMenor;
            }

            // Llama a una función para mostrar los datos apropiadamente;
            Herramientas.MostrarArrayEnteros(valoresMenores, "Valores mayores por columna", "Columna {0}: {1}");
        }

        /// <summary>
        /// Permite calcular el valor más alto en una fila de una matriz.
        /// </summary>
        public static void ValoresMayoresFilaMatriz()
        {
            Console.WriteLine("Calcula el valor máximo de cada fila perteneciente a una matriz.\n");

            // Declaración de variables;
            int[,] valoresIngresados;
            int[] valoresMayores;
            int columnas, filas, valor, valorMayor = 0;

            // Obtiene el número de filas y columnas;
            columnas = Herramientas.IngresarEnteroMayorA_0("Número de columnas: ");
            filas = Herramientas.IngresarEnteroMayorA_0("Filas: ");

            // Estableciendo el tamaño de la matriz de valores y del array de los valores maximos;
            valoresIngresados = new int[columnas, filas];
            valoresMayores = new int[filas];

            // Proceso columna por columna;
            for (int c = 0; c < columnas; c++)
            {
                Console.WriteLine("\nColumna {0}", c + 1);

                // Obtiene los datos fila por fila;
                for (int f = 0; f < filas; f++)
                {
                    valoresIngresados[c, f] = Herramientas.IngresarEntero(string.Format("Valor {0}: ", f + 1));
                }
            }

            // Obtiene los valores máximos, esta vez procesando fila por fila en primer lugar;
            for (int f = 0; f < filas; f++)
            {
                for (int c = 0; c < columnas; c++)
                {
                    valor = valoresIngresados[c, f];

                    if (c == 0)
                    {
                        valorMayor = valor;
                    }
                    else if (valor > valorMayor)
                    {
                        valorMayor = valor;
                    }
                }

                valoresMayores[f] = valorMayor;
            }

            Herramientas.MostrarArrayEnteros(valoresMayores, "Valores mayores por fila", "Fila {0}: {1}");
        }

        public static void OrdenarValores()
        {
            int[,] valoresIngresados, valoresOrdenados;
            int[] valoresColumna;

            int columnas, filas;

            columnas = Herramientas.IngresarEnteroMayorA_0("Número de columnas: ");
            filas = Herramientas.IngresarEnteroMayorA_0("Filas: ");
            valoresIngresados = new int[columnas, filas];
            valoresOrdenados = new int[columnas, filas];

            for (int c = 0; c < columnas; c++)
            {
                Console.WriteLine($"\nColumna {c + 1}");
                valoresColumna = new int[filas];

                for (int f = 0; f < filas; f++)
                {
                    valoresColumna[f] = valoresIngresados[c, f] = Herramientas.IngresarEntero($"Valor {f + 1}: ");
                }

                Array.Sort(valoresColumna);

                for (int f = 0; f < filas; f++)
                {
                    valoresOrdenados[c, f] = valoresColumna[f];
                }
            }

            Console.WriteLine("\nMostrando los valores ordenados ascendentemente:");
            for (int c = 0; c < columnas; c++)
            {
                Console.Write($"C-{c + 1}" + "\t");
            }
            Console.WriteLine();

            for (int f = 0; f < filas; f++)
            {
                for (int c = 0; c < columnas; c++)
                {
                    Console.Write(valoresOrdenados[c, f] + "\t");
                }
                Console.WriteLine();
            }
        }

    }
}
