﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramaMatrices
{
    class Empleados
    {
        private static string[] empleados;
        private static int[,] sueldos;
        private static int[] sueldosAcumulados;

        /// <summary>
        /// Calcula quien obtuvo el mayor sueldo en un lapso de tiempo establecido.
        /// </summary>
        /// <param name="cantidadEmpleados">Número de empleados.</param>
        /// <param name="meses">Meses.</param>
        public static void ProcesarEmpleados(int cantidadEmpleados, int meses)
        {
            // Texto de información;
            Console.WriteLine($"Se procesará el sueldo de {cantidadEmpleados} empleados en {meses} meses.\n");

            // Declaración de variables, arreglos y matrices;
            empleados = new string[cantidadEmpleados];
            sueldos = new int[cantidadEmpleados, meses];
            sueldosAcumulados = new int[cantidadEmpleados];

            int sueldoMes, empleadoMayorSueldo;
            int totalSalario = 0;

            // Solicitud del nombre de cada empleado;
            for (int i = 0; i < cantidadEmpleados; i++)
            {
                // Obtiene el nombre del empleado;
                empleados[i] = Herramientas.IngresarCadena($"Ingrese el nombre del empleado {i + 1}: ");

                // Solicitud de los sueldos en los meses especificados;
                for (int j = 0; j < meses; j++)
                {
                    // Obtiene el sueldo para el mes;
                    sueldoMes = Herramientas.IngresarEntero($"Sueldo mes {j + 1}: ");
                    sueldos[i, j] = sueldoMes;

                    // Almacena el sueldo acumulado en el vector;
                    sueldosAcumulados[i] += sueldoMes;
                }

                // Totaliza los sueldos acumulados de todos los empleados;
                totalSalario += sueldosAcumulados[i];

                Console.WriteLine();
            }

            // Obtiene el empleado de mayor sueldo;
            empleadoMayorSueldo = EmpleadoMayorSueldo();

            // Resumen de la información solicitada;
            Console.WriteLine(
                $"Sueldo total pagado a los empleados en los últimos {meses} meses: ${totalSalario}\n" +
                $"Empleado con el mayor sueldo: {empleados[empleadoMayorSueldo]} | Sueldo: ${sueldosAcumulados[empleadoMayorSueldo]}"
                );
        }

        /// <summary>
        /// Obtiene el identificador del empleado com mayor sueldo.
        /// </summary>
        /// <returns>Ubicación en la matriz.</returns>
        private static int EmpleadoMayorSueldo()
        {
            int sueldoMayor, sueldo, empleado;
            sueldoMayor = 0;
            empleado = 0;

            for (int i = 0; i < empleados.Length; i++)
            {
                sueldo = sueldosAcumulados[i];

                if (sueldo > sueldoMayor)
                {
                    empleado = i;
                    sueldoMayor = sueldo;
                }
            }

            return empleado;
        }

    }
}
